﻿//-----------------------------------------------------------------------
// <copyright file="LoggerTest.cs" company="CompanyName">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

using NUnit.Framework;
using RES;
using System;
using System.IO;

namespace LoggerTest
{
    [TestFixture]
    public class LoggerTest
    {
        StreamWriter badWriter;
        StreamReader sr;
        [SetUp]
        public void Setup()
        {
            badWriter = null;
            sr = null;
        }

        [Test]
        [TestCase("class", "method", "logmessage")]
        public void DobarLog(string className, string method, string logMessage)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Assert.DoesNotThrow(() =>
                {
                    Logger.Log(className, method, logMessage, w);
                });
            }                        
        }

        [Test]
        [TestCase("class", "method", "logmessage")]        
        public void BadWriterLog(string className, string method, string logMessage)
        {            
            Assert.Throws<ArgumentNullException>(() =>
            {
                Logger.Log("class", "method", "logmessage", badWriter);
            });            
        }

        [Test]
        [TestCase(null, "method", "logmessage")]
        [TestCase(null, null, "logmessage")]
        [TestCase(null, null, null)]
        public void ArgumentNullLog(string className, string method, string logMessage)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Assert.Throws<ArgumentNullException>(() =>
                {
                Logger.Log(className, method, logMessage, w);
                });
            }
        }

        [Test]
        public void DobarReadLog()
        {
            using (StreamReader reader = new StreamReader("log.txt"))
            {
                Assert.DoesNotThrow(() =>
                {
                    Logger.DumpLog(reader);
                });
            }
        }

        [Test]
        public void LosReadLog()
        {            
            Assert.Throws<ArgumentNullException>(() =>
            {
                Logger.DumpLog(sr);
            });            
        }
    }
}
