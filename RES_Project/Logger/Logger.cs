﻿//-----------------------------------------------------------------------
// <copyright file="Logger.cs" company="CompanyName">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;

namespace RES
{
    /// <summary>
    ///  Logger class is used for making log files and monitoring all data
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Method for making logs
        /// </summary>
        /// <param name="className">Represents the name of the class making the log</param>
        /// <param name="method">Represents the name of the method  making the log</param>
        /// <param name="logMessage">Message to be logged</param>
        /// <param name="w">TextWriter instance</param>
        public static void Log(string className, string method, string logMessage, TextWriter w)
        {                      
            if (className == null || method == null || logMessage == null || w == null)
            {
                throw new ArgumentNullException("Parameter is missing or it is null");
            }
           
            if (className == string.Empty || method == string.Empty || logMessage == string.Empty)
            {
                throw new ArgumentException("Parameters can't be empty text");
            }

            w.Write("\r\nLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
            w.WriteLine("Class  :{0}", className);
            w.WriteLine("Function  :{0}", method);
            w.WriteLine("Message  :{0}", logMessage);
            w.WriteLine("-------------------------------");            
        }

        /// <summary>
        /// Method for reading logs
        /// </summary>
        /// <param name="r">StreamReader instance</param>
        public static void DumpLog(StreamReader r)
        {
            if (r == null)
            {
                throw new ArgumentNullException("Argument can't be null");
            }

            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }
    }
}
