﻿//-----------------------------------------------------------------------
// <copyright file="Enums.cs" company="CompanyName">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

/// <summary>
/// Codes enumerations 
/// </summary>
public enum Codes
{
    Analog,
    Digital,
    Custom,
    LimitSet,
    Singlenoe,
    MultipleNode,
    Consumer,
    Source
}